<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticesTable extends Migration
{
    public function up()
    {
        Schema::create('notices', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('title');
            $table->text('body');
            $table->date('start');
            $table->date('end');
            $table->date('archive')->nullable();
            $table->enum('type', ['important', 'normal']);
        });

        Schema::create('notice_user', function (Blueprint $table) {
            $table->integer('notice_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->boolean('read')->default(false);
            $table->datetime('date');

            $table->primary(['notice_id', 'user_id']);

            $table->foreign('notice_id')->references('id')->on('notices')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('notice_user');
        Schema::dropIfExists('notices');
    }
}
