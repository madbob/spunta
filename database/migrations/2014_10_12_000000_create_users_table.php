<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->rememberToken();
            $table->softDeletes();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('permissions')->default('["user"]');
            $table->boolean('remember')->default(false);
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
