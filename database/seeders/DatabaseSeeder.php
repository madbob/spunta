<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Hash;
use Carbon\Carbon;
use App\User;
use App\Config;
use App\Notice;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        if (User::count() == 0) {
            $user = new User();
            $user->name = 'Amministratore';
            $user->email = 'admin@example.com';
            $user->password = Hash::make('admin');
            $user->permissions = '["users","checklists","notices"]';
            $user->save();
        }

        Config::getConfig('intro_message');
        $conf = Config::where('name', 'intro_message')->first();
        $conf->value = "Per accedere usa le credenziali\nadmin@example.com / admin\n\nI contenuti di questa istanza demo vengono azzerati e ripristinati alla versione originale ogni giorno. Non introdurre dati reali o sensibili: questa istanza è potenzialmente consultabile da chiunque!";
        $conf->save();

        if (Notice::count() == 0) {
            $notice = new Notice();
            $notice->title = 'Benvenuto su Spunta!';
            $notice->body = 'Clicca le opzioni del menu in alto per esplorare le funzioni e caricare i contenuti.';
            $notice->start = Carbon::now();
            $notice->end = Carbon::now()->addDays(15);
            $notice->type = 'important';
            $notice->archive = null;
            $notice->save();
        }
    }
}
