<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class Notice extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot(['date']);
    }

    public function scopeActive($query)
    {
        $now = date('Y-m-d');
        return $query->where(DB::raw('DATE(start)'), '<=', $now)->where(DB::raw('DATE(end)'), '>=', $now);
    }

    public function isRead()
    {
        if ($this->type == 'important') {
            return false;
        }

        $user = Auth::user();
        return ($this->users()->where('user_id', $user->id)->count() == 1);
    }
}
