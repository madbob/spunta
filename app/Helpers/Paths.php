<?php

function global_multi_installation()
{
    return true;
}

function instance_name()
{
    return substr($_SERVER['HTTP_HOST'], 0, strpos($_SERVER['HTTP_HOST'], '.'));
}

function keys_path()
{
    if (global_multi_installation()) {
        $instance = instance_name();
        $ret = storage_path('app/keys/' . $instance . '/');

        if (file_exists($ret) == false) {
            mkdir($ret, 0766);
        }
    }
    else {
        $ret = storage_path('app/keys/');
    }

    return $ret;
}

function images_path()
{
    if (global_multi_installation()) {
        $instance = instance_name();
        $ret = public_path('/images/' . $instance . '/');

        if (file_exists($ret) == false) {
            mkdir($ret, 0766);
        }
    }
    else {
        $ret = public_path('/images/');
    }

    return $ret;
}

function env_file()
{
    if (global_multi_installation() && isset($_SERVER['HTTP_HOST'])) {
        $instance = instance_name();
        return ('.env.' . $instance);
    }
    else {
        return '.env';
    }
}
