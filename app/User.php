<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mail;
use OpenPGP;
use OpenPGP_Message;
use OpenPGP_PublicKeyPacket;
use App\Mail\KeysReset;
use App\Config;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hasPermission($permission)
    {
        $current = json_decode($this->permissions);

        foreach ($current as $c) {
            if ($c == $permission) {
                return true;
            }
        }

        return false;
    }

    public function saveKeys($pubkey, $privkey)
    {
        $priv_path = keys_path() . $this->id;
        $pub_path = keys_path() . $this->id . '.pub';

        if (file_exists($priv_path) || file_exists($pub_path)) {
            abort(403);
        }

        file_put_contents($priv_path, $privkey);
        file_put_contents($pub_path, $pubkey);

        $mail = Config::getConfig('notify_sync_reports');
        if (!empty($mail)) {
            $key_id = '???';
            $data = OpenPGP::unarmor($pubkey, 'PGP PUBLIC KEY BLOCK');
            $key = OpenPGP_Message::parse($data);
            foreach ($key as $p) {
                if ($p instanceof OpenPGP_PublicKeyPacket) {
                    $key_id = $p->key_id;
                    break;
                }
            }

            try {
                Mail::to($mail)->send(new KeysReset($this, $key_id));
            }
            catch (\Exception $e) {
                \Log::error('Unable to send key reset notification: ' . $e->getMessage());
            }
        }
    }

    public function deleteKeys()
    {
        $priv_path = keys_path() . $this->id;
        @unlink($priv_path);
        $pub_path = keys_path() . $this->id . '.pub';
        @unlink($pub_path);
    }

    public function getPrivkeyAttribute()
    {
        $path = keys_path() . $this->id;
        if (file_exists($path)) {
            return file_get_contents($path);
        }
        else {
            return null;
        }
    }
}
