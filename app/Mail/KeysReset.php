<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class KeysReset extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $user;
    public $key_id;

    public function __construct($user, $key_id)
    {
        $this->user = $user;
        $this->key_id = $key_id;
    }

    public function build()
    {
        return $this->subject(__('email.subjects.reset', ['user' => $this->user->name]))->replyTo($this->user->email)->text('email.keysreset');
    }
}
