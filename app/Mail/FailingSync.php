<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FailingSync extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $report;
    public $message;

    public function __construct($report, $message)
    {
        $this->report = $report;
        $this->message = $message;
    }

    public function build()
    {
        return $this->subject(__('email.subjects.failsync'))->text('email.failingsync');
    }
}
