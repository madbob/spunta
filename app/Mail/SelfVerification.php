<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SelfVerification extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $filepath;
    public $report;

    public function __construct($filepath, $report)
    {
        $this->filepath = $filepath;
        $this->report = $report;
    }

    public function build()
    {
        return $this->subject(__('email.subjects.send_back'))
            ->attachFromStorageDisk(env('REPORTS_STORAGE'), $this->filepath)
            ->replyTo($this->report->user->email)
            ->text('email.selfverification');
    }
}
