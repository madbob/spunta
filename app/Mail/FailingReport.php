<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FailingReport extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $report;
    public $issues;

    public function __construct($report, $issues)
    {
        $this->report = $report;
        $this->issues = $issues;
    }

    public function build()
    {
        return $this->subject(__('email.subjects.anomaly', ['checklist' => $this->report->checklist->name]))->replyTo($this->report->user->email)->text('email.failingreport');
    }
}
