<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FailingTiming extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $report;

    public function __construct($report)
    {
        $this->report = $report;
    }

    public function build()
    {
        return $this->subject(__('email.subjects.out_of_time', ['checklist' => $this->report->checklist->name]))->replyTo($this->report->user->email)->text('email.failingtiming');
    }
}
