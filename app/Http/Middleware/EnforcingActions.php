<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\History;

class EnforcingActions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user = Auth::user();

        if ($user) {
            if ($user->privkey == null) {
                if ($request->route()->getName() != 'user.keys') {
                    return redirect()->route('user.keys');
                }
            }
            else {
                $report = History::completed()->where('user_id', $user->id)->where('signed', false)->first();
                if ($report != null) {
                    return redirect()->route('history.sign', $report->id);
                }
            }
        }

        return $next($request);
    }
}
