<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Item;

class ItemController extends Controller
{
    private function commonSave($item, $request)
    {
        $item->name = $request->input('name');
        $item->help = $request->input('help');
        $item->type = $request->input('type');
        $item->notify = $request->has('notify');
        $item->mandatory = $request->has('mandatory');
        $item->data = json_encode(array_filter($request->input('option', [])));

        if ($item->notify) {
            switch ($item->type) {
                case 'boolean':
                    $notification = (object) [
                        'when' => $request->input('boolean_value'),
                    ];
                    break;

                case 'number':
                    $notification = (object) [
                        'when' => $request->input('number_value_direction'),
                        'than' => $request->input('number_value_threeshold'),
                    ];
                    break;

                case 'select':
                    $notification = (object) [
                        'when' => array_filter($request->input('notifiable', [])),
                    ];
                    break;

                case 'text':
                case 'longtext':
                    $notification = (object) [
                        'when' => $request->input('text_value'),
                    ];
                    break;

                default:
                    $notification = (object) [];
                    break;
            }
        }
        else {
            $notification = (object) [];
        }

        $item->notification = json_encode($notification);

        $item->save();
    }

    public function store(Request $request)
    {
        $this->permissionAccess('checklists');
        $item = new Item();
        $this->commonSave($item, $request);
        return redirect()->route('category.index', ['tab' => 'items']);
    }

    public function update(Request $request, $id)
    {
        $this->permissionAccess('checklists');
        $item = Item::findOrFail($id);
        $this->commonSave($item, $request);
        return redirect()->route('category.index', ['tab' => 'items']);
    }

    public function edit(Request $request, $id)
    {
        $this->permissionAccess('checklists');
        $item = Item::findOrFail($id);
        return view('item.edit', compact('item'));
    }

    public function destroy($id)
    {
        $this->permissionAccess('checklists');
        Item::findOrFail($id)->delete();
        return redirect()->route('category.index', ['tab' => 'items']);
    }

    public function askdestroy($id)
    {
        $this->permissionAccess('checklists');
        $item = Item::findOrFail($id);
        return view('item.destroy', compact('item'));
    }

    public function importSample(Request $request)
    {
        $this->permissionAccess('checklists');

        return response()->streamDownload(function () {
            $columns = [];

            $options = Item::importColumns();
            foreach ($options as $opt) {
                $columns[] = $opt->label;
            }

            echo join(',', $columns) . "\n";

            $samples = [
                ['Autista', '', 'Testo Breve', ''],
                ['Termometro', '', 'Vero/Falso', ''],
                ['Bombola O2 Fissa', 'Segnare pressione', 'Numerico', ''],
                ['Rasoio Monouso', 'Devono essere 2', 'Vero/Falso', ''],
                ['Pneumatici', 'Usura e pressione apparente', 'Scelta', '"OK, Non OK"'],
                ['Note', '', 'Testo Lungo', ''],
            ];

            foreach ($samples as $opt) {
                echo join(',', $opt) . "\n";
            }

        }, 'template.csv');
    }

    public function import(Request $request)
    {
        $this->permissionAccess('checklists');

        $path = $request->file->store('csv', 'local');
        $filepath = Storage::disk('local')->path($path);
        $file = fopen($filepath, 'r');

        $comma_rows = fgetcsv($file, ',');
        $colon_rows = fgetcsv($file, ';');
        if (count($comma_rows) > count($colon_rows)) {
            $separator = ',';
        }
        else {
            $separator = ';';
        }

        rewind($file);
        fgetcsv($file, $separator);

        while ($row = fgetcsv($file, $separator)) {
            try {
                $name = trim($row[0]);

                $item = Item::where('name', $name)->first();
                if (is_null($item)) {
                    $item = new Item();
                    $item->name = $name;
                    $item->notify = false;
                }

                $item->help = trim($row[1]);

                $type = mb_strtolower(trim($row[2]));
                $options = [];

                if (Str::startsWith($type, 'numer')) {
                    $type = 'number';
                }
                elseif (Str::startsWith($type, 'bool') || Str::startsWith($type, 'vero')) {
                    $type = 'boolean';
                }
                elseif (Str::startsWith($type, 'dat')) {
                    $type = 'datetime';
                }
                elseif (Str::contains($type, 'testo') || Str::contains($type, 'text')) {
                    if (Str::contains($type, 'breve') || Str::contains($type, 'short')) {
                        $type = 'text';
                    }
                    else {
                        $type = 'longtext';
                    }
                }
                elseif (Str::startsWith($type, 'select') || Str::startsWith($type, 'choice') || Str::startsWith($type, 'scelta')) {
                    $type = 'select';

                    foreach (explode(',', $row[3]) as $opt) {
                        $options[] = trim($opt);
                    }
                }
                else {
                    throw new \Exception('Unrecognized type: ' . $type, 1);
                }

                $item->type = $type;
                $item->data = json_encode($options);
                $item->save();
            }
            catch (\Exception $e) {
                // dummy
            }
        }

        fclose($file);
        unlink($filepath);

        return redirect()->route('category.index', ['tab' => 'items']);
    }
}
