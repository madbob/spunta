<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Auth;
use Mail;
use Log;
use DB;
use Storage;
use ReportsDisk;
use App\Mail\FailingReport;
use App\Mail\FailingTiming;
use App\Mail\FailingSync;
use App\Mail\SelfVerification;
use App\Checklist;
use App\History;
use App\Config;

class HistoryController extends Controller
{
    private function searchQuery($request)
    {
        $query = History::completed()->orderBy('date', 'desc');

        $actual_user = $request->input('user_id', -1);
        if ($actual_user != -1) {
            $query->where('user_id', $actual_user);
        }

        $actual_checklist = $request->input('checklist_id', -1);
        if ($actual_checklist != -1) {
            $query->where('checklist_id', $actual_checklist);
        }

        $actual_start = $request->input('start', '');
        if ($actual_start != '') {
            $query->where(DB::raw('DATE(date)'), '>=', $actual_start);
        }

        $actual_end = $request->input('end', '');
        if ($actual_end != '') {
            $query->where(DB::raw('DATE(date)'), '<=', $actual_end);
        }

        $actual_hour_start = $request->input('hour_start', -1);
        if ($actual_hour_start != -1) {
            $query->where(DB::raw('TIME(date)'), '>=', $actual_hour_start);
        }

        $actual_hour_end = $request->input('hour_end', -1);
        if ($actual_hour_end != -1) {
            $query->where(DB::raw('TIME(date)'), '<=', $actual_hour_end);
        }

        return [$actual_user, $actual_checklist, $actual_start, $actual_end, $actual_hour_start, $actual_hour_end, $query];
    }

    private function explodeHistory(&$contents, $data)
    {
        foreach ($data->contents as $content) {
            if (isset($content->contents)) {
                $this->explodeHistory($contents, $content);
            }
            else {
                $contents[] = $content->name;
                $contents[] = $content->value;
            }
        }
    }

    public function index(Request $request)
    {
        History::where(DB::raw('DATE(date)'), '<', date('Y-m-d', strtotime('-1 days')))->completed()->where('signed', false)->delete();
        list($actual_user, $actual_checklist, $actual_start, $actual_end, $actual_hour_start, $actual_hour_end, $query) = $this->searchQuery($request);
        $reports = $query->paginate(50);
        return view('history.index', compact('reports', 'actual_user', 'actual_checklist', 'actual_start', 'actual_end', 'actual_hour_start', 'actual_hour_end'));
    }

    public function export(Request $request)
    {
        list($actual_user, $actual_checklist, $actual_start, $actual_end, $actual_hour_start, $actual_hour_end, $query) = $this->searchQuery($request);
        $reports = $query->get();

        $callback = function () use ($reports) {
            $f = fopen('php://output', 'w');

            foreach ($reports as $report) {
                $contents = [
                    $report->user->name,
                    $report->checklist->name,
                    printableDateTime($report->date),
                ];

                $data = json_decode($report->as_data);
                $this->explodeHistory($contents, $data);

                fputcsv($f, $contents);
            }

            fclose($f);
        };

        return response()->streamDownload($callback, 'reports.csv', [
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type' => 'text/csv',
            'Expires' => '0',
            'Pragma' => 'public'
        ]);
    }

    public function create(Request $request)
    {
        $checklist_id = $request->input('checkid', -1);
        $checklist = Checklist::find($checklist_id);
        if ($checklist == null) {
            return redirect()->route('checklist.index');
        }

        return view('history.create', compact('checklist'));
    }

    public function edit(Request $request, $id)
    {
        $user_id = $request->user()->id;
        $pending = History::find($id);
        if ($pending == null || $pending->pending == false || $pending->user_id != $user_id) {
            return redirect()->route('checklist.index');
        }

        $checklist = $pending->checklist;
        return view('history.create', compact('checklist', 'pending'));
    }

    private function iterateSlot($slot, $request, &$issues, $deep)
    {
        $sdata = (object) [
            'name' => $slot->name,
            'contents' => []
        ];

        $header = str_repeat('##', $deep);
        $text = sprintf("\n%s %s\n", $header, $slot->name);

        foreach ($slot->contents as $content) {
            if (is_a($content, 'App\Item')) {
                $value = $request->input(sprintf('item_%d', $content->id));
                $status = $content->evaluate($value);

                $sdata->contents[] = (object) [
                    'name' => $content->name,
                    'value' => $value,
                    'status' => !$status,
                ];

                $text .= sprintf("%s: %s\n", $content->name, $value);

                if ($status == true && $content->notify == true) {
                    $issues[] = sprintf("%s: %s", $content->name, $value);
                }
            }
            elseif (is_a($content, 'App\Slot')) {
                list($subdata, $subtext) = $this->iterateSlot($content, $request, $issues, $deep + 1);
                $sdata->contents[] = $subdata;
                $text .= $subtext;
            }
        }

        return [$sdata, $text];
    }

    public function store(Request $request)
    {
        $checklist_id = $request->input('checklist_id');
        $checklist = Checklist::find($checklist_id);
        if ($checklist == null) {
            return redirect()->route('checklist.index');
        }

        $pending_id = $request->input('pending_id', 0);
        $user = $request->user();

        if ($pending_id) {
            $report = History::find($pending_id);
            if ($report->user_id != $user->id || $report->pending == false) {
                return redirect()->route('checklist.index');
            }
        }
        else {
            $report = new History();
            $report->user_id = $user->id;
            $report->checklist_id = $checklist_id;
        }

        $report->date = date('Y-m-d G:i:s');

        $data = (object) [
            'name' => $checklist->name,
            'contents' => []
        ];

        $text = sprintf("# %s\n# %s\n# %s\n", $checklist->name, $user->name, date('d/m/Y H:i:s'));
        $issues = [];

        foreach ($checklist->slots as $slot) {
            list($sdata, $subtext) = $this->iterateSlot($slot, $request, $issues, 1);
            $text .= $subtext;
            $data->contents[] = $sdata;
        }

        $action = $request->input('action', 'submit');

        switch ($action) {
            case 'submit':
                $report->pending = false;
                $report->status = empty($issues);
                $report->as_data = json_encode($data);
                $report->as_text = $text;
                $report->save();

                if (!empty($checklist->notices_recipient)) {
                    if (Config::getConfig('checklist_times')) {
                        if ($checklist->inTime() == false) {
                            $recipients = explode(',', $checklist->notices_recipient);
                            foreach ($recipients as $r) {
                                try {
                                    $r = trim($r);
                                    Mail::to($r)->send(new FailingTiming($report));
                                }
                                catch (\Exception $e) {
                                    Log::error('Unable to send mail to checklist referent about failing time: ' . $e->getMessage());
                                }
                            }
                        }
                    }

                    if ($report->status == false) {
                        $recipients = explode(',', $checklist->notices_recipient);
                        foreach ($recipients as $r) {
                            try {
                                $r = trim($r);
                                Mail::to($r)->send(new FailingReport($report, $issues));
                            }
                            catch (\Exception $e) {
                                Log::error('Unable to send mail to checklist referent about failing items: ' . $e->getMessage());
                            }
                        }
                    }
                }

                $no_menu = true;

                return view('history.sign', compact('report', 'user', 'no_menu'));

            case 'pending':
                $report->pending = true;
                $report->as_data = json_encode($data);
                $report->as_text = '';
                $report->save();
                return redirect()->route('checklist.index');
                break;
        }
    }

    /*
        This function is used to complete the report saving flow, in particular
        after a key reset
    */
    public function sign($id)
    {
        $report = History::findOrFail($id);
        $user = Auth::user();
        $no_menu = true;
        return view('history.sign', compact('report', 'user', 'no_menu'));
    }

    public function show($id)
    {
        $report = History::findOrFail($id);
        return view('history.show', compact('report'));
    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $report = History::findOrFail($id);

        if ($report->user_id != $user->id) {
            abort(403);
        }

        $report->as_text = $request->input('signed');
        $report->signed = true;
        $report->save();

        $path = $report->toPDF();

        $ftp = Config::getConfig('ftp_push');
        if ($ftp->enable) {
            try {
                $disk = Storage::build([
                    'driver' => 'ftp',
                    'host' => $ftp->host,
                    'username' => $ftp->username,
                    'password' => $ftp->password,
                    'port' => (int) $ftp->port,
                    'passive' => $ftp->passive,
                    'ssl' => $ftp->ssl,
                    'root' => $ftp->root,
                    'timeout' => 20,
                ]);

                $disk->put(basename($path), ReportsDisk::get($path));
            }
            catch (\Exception $e) {
                Log::error('Unable to save report to FTP server: ' . $e->getMessage());
            }
        }

        if (Config::getConfig('self_verification')) {
            try {
                Mail::to($user->email)->send(new SelfVerification($path, $report));
            }
            catch (\Exception $e) {
                Log::error('Unable to send mail to checklist user for self-verification: ' . $e->getMessage());
            }
        }
    }

    public function download($id)
    {
        $report = History::findOrFail($id);
        $path = $report->toPDF();
        return ReportsDisk::download($path);
    }
}
