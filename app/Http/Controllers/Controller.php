<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\Config;

class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    public function __construct()
    {
        $language = Config::getConfig('language');
        App::setLocale(explode('_', $language)[0]);
    }

    protected function permissionAccess($permissions)
    {
        $user = Auth::user();

        if (!is_array($permissions)) {
            $permissions = [$permissions];
        }

        foreach ($permissions as $p) {
            if ($user->hasPermission($p)) {
                return;
            }
        }

        abort(403);
    }
}
