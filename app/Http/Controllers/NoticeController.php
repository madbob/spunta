<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Notice;

class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notices = Notice::active()->where(function ($query) {
            $query->where('type', 'important')->orWhere(function ($query) {
                $query->whereDoesntHave('users', function ($query) {
                    $query->where('user_id', Auth::user()->id);
                });
            });
        })->orderByRaw("FIELD(type, 'important', 'normal')")->get();

        return view('notice.index', compact('notices'));
    }

    public function archive()
    {
        $now = date('Y-m-d');
        $notices = Notice::where(DB::raw('DATE(archive)'), '>', $now)->orWhereNull('archive')->orderBy('start', 'desc')->paginate(10);
        return view('notice.archive', compact('notices', 'now'));
    }

    public function admin()
    {
        $this->permissionAccess('notices');
        $notices = Notice::orderBy('start', 'desc')->paginate(100);
        $now = date('Y-m-d H:i:s');
        return view('notice.admin', compact('notices', 'now'));
    }

    public function readcount($id)
    {
        $notice = Notice::findOrFail($id);
        return view('notice.readcount', compact('notice'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->permissionAccess('notices');

        $notice = new Notice();
        $notice->title = $request->input('title');
        $notice->body = $request->input('body');
        $notice->start = $request->input('start');
        $notice->end = $request->input('end');
        $notice->type = $request->input('type');

        $notice->archive = $request->input('archive');
        if (empty($notice->archive)) {
            $notice->archive = null;
        }

        $notice->save();

        return redirect()->route('notice.admin');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->permissionAccess('notices');
        $notice = Notice::findOrFail($id);
        return view('notice.edit', compact('notice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->permissionAccess('notices');

        $notice = Notice::findOrFail($id);
        $notice->title = $request->input('title');
        $notice->body = $request->input('body');
        $notice->start = $request->input('start');
        $notice->end = $request->input('end');
        $notice->type = $request->input('type');

        $notice->archive = $request->input('archive');
        if (empty($notice->archive)) {
            $notice->archive = null;
        }

        $notice->save();

        return redirect()->route('notice.admin');
    }

    public function read($id)
    {
        try {
            $notice = Notice::findOrFail($id);
            $notice->users()->attach(Auth::user()->id, ['read' => true, 'date' => date('Y-m-d H:i:s')]);
        }
        catch (\Exception $e) {
            /*
                This usually happens when a user marks again a notice as read.
                That's ok.
            */
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->permissionAccess('notices');
        Notice::findOrFail($id)->delete();
        return redirect()->route('notice.admin');
    }

    public function askdestroy($id)
    {
        $this->permissionAccess('notices');
        $notice = Notice::findOrFail($id);
        return view('notice.destroy', compact('notice'));
    }
}
