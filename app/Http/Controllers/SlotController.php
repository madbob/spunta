<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Checklist;
use App\Slot;

class SlotController extends Controller
{
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request)
    {
        $this->permissionAccess('checklists');
        $parent_id = $request->input('parent_id');
        $parent_slot = Slot::findOrFail($parent_id);
        return view('slot.edit', compact('parent_slot'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $this->permissionAccess('checklists');

        $checklist_id = $request->input('checklist_id');
        $checklist = Checklist::findOrFail($checklist_id);

        $slot = new Slot();
        $slot->name = $request->input('name');
        $slot->checklist_id = $checklist_id;
        $slot->parent_id = $request->input('parent_id');
        $slot->sorting = $checklist->slots()->count() + 1;
        $slot->save();

        return redirect()->route('checklist.show', $slot->checklist_id);
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $this->permissionAccess('checklists');
        $slot = Slot::findOrFail($id);
        return view('slot.edit', compact('slot'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $this->permissionAccess('checklists');

        $saving_type = $request->input('saving_type', '');

        switch ($saving_type) {
            case 'sort_items':
                $slot = Slot::findOrFail($id);

                $index = 1;
                $ids = $request->input('ids');
                foreach ($ids as $id) {
                    if (strpos($id, 'item_') === 0) {
                        $id = substr($id, 5);
                        $slot->items()->updateExistingPivot($id, ['sorting' => $index]);
                    }
                    elseif (strpos($id, 'slot_') === 0) {
                        $id = substr($id, 5);
                        $slot->slots()->where('id', $id)->update(['sorting' => $index]);
                    }

                    $index++;
                }
                break;

            default:
                $slot = Slot::findOrFail($id);
                $slot->name = $request->input('name');
                $slot->save();
                return redirect()->route('checklist.show', $slot->checklist_id);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $this->permissionAccess('checklists');
        $slot = Slot::findOrFail($id);
        $checklist_id = $slot->checklist_id;
        $slot->delete();
        return redirect()->route('checklist.show', $checklist_id);
    }

    public function askdestroy($id)
    {
        $this->permissionAccess('checklists');
        $slot = Slot::findOrFail($id);
        return view('slot.destroy', compact('slot'));
    }

    public function attach(Request $request, $id)
    {
        $this->permissionAccess('checklists');

        $slot = Slot::findOrFail($id);
        $item_id = $request->input('item');

        $attach = $request->input('revert', 'false');
        if ($attach == 'false') {
            if ($slot->items()->where('items.id', $item_id)->count() == 0) {
                $slot->items()->attach($item_id, ['sorting' => $slot->items()->count() + 1]);
            }
        }
        else {
            $slot->items()->detach($item_id);
        }

        return response()->json('ok');
    }
}
