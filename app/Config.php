<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Config;

class Config extends Model
{
    private static function defaultConfig($name)
    {
        $configs = [
            'checklist_times' => [
                'type' => 'boolean',
                'default' => 'false'
            ],
            'self_verification' => [
                'type' => 'boolean',
                'default' => 'true'
            ],
            'notify_sync_reports' => [
                'type' => 'string',
                'default' => ''
            ],
            'language' => [
                'type' => 'string',
                'default' => 'it_IT'
            ],
            'login' => [
                'type' => 'string',
                'default' => 'manual'
            ],
            'enforced_domain' => [
                'type' => 'string',
                'default' => ''
            ],
            'intro_message' => [
                'type' => 'string',
                'default' => ''
            ],
            'ftp_push' => [
                'type' => 'object',
                'default' => json_encode([
                    'enable' => false,
                    'host' => '',
                    'username' => '',
                    'password' => '',
                    'port' => 21,
                    'passive' => '',
                    'ssl' => '',
                    'root' => '',
                ]),
            ],
        ];

        $ret = new Config();
        $ret->name = $name;
        $ret->type = $configs[$name]['type'];
        $ret->value = $configs[$name]['default'];
        return $ret;
    }

    public static function getConfig($name)
    {
        $conf = Config::where('name', $name)->first();
        if ($conf == null) {
            $conf = Config::defaultConfig($name);
            $conf->save();
        }

        switch ($conf->type) {
            case 'string':
            case 'number':
                return $conf->value;

            case 'object':
                return json_decode($conf->value);

            case 'boolean':
                return ($conf->value == 'true');
        }

        return '';
    }
}
