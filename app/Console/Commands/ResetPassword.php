<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use App\User;

class ResetPassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:reset-password {userid} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $userid = $this->argument('userid');
        $user = User::find($userid);
        if ($user) {
            $password = $this->argument('password');
            $user->password = Hash::make($password);
            $user->save();
            $this->info('OK');
        }
        else {
            $this->info('User not found');
        }
    }
}
