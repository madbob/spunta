<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

class Slot extends Model implements MayHaveValue
{
    use SoftDeletes;

    public function checklist()
    {
        return $this->belongsTo('App\Checklist');
    }

    public function slots()
    {
        return $this->hasMany('App\Slot', 'parent_id', 'id');
    }

    public function items()
    {
        return $this->belongsToMany('App\Item')->withPivot('sorting')->orderBy('item_slot.sorting');
    }

    public function getContentsAttribute()
    {
        $slots = $this->slots;
        $items = $this->items;

        $contents = new Collection();
        $contents = $contents->merge($slots);
        $contents = $contents->merge($items);
        $contents = $contents->sort(function ($a, $b) {
            $a_sorting = is_a($a, 'App\Slot') ? $a->sorting : $a->pivot->sorting;
            $b_sorting = is_a($b, 'App\Slot') ? $b->sorting : $b->pivot->sorting;
            return $a_sorting <=> $b_sorting;
        });

        return $contents;
    }

    public function existingData($data)
    {
        if ($data) {
            foreach ($data as $d) {
                if ($d->name == $this->name && isset($d->contents)) {
                    return $d->contents;
                }
            }
        }

        return false;
    }
}
