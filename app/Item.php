<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model implements MayHaveValue
{
    use SoftDeletes;

    public static function datatypes()
    {
        return [
            (object) [
                'value' => 'boolean',
                'label' => __('commons.item.types.labels.boolean'),
                'notifiable' => true,
                'help' => __('commons.item.types.hints.boolean'),
            ],
            (object) [
                'value' => 'number',
                'label' => __('commons.item.types.labels.numeric'),
                'notifiable' => true,
                'help' => __('commons.item.types.hints.numeric'),
            ],
            (object) [
                'value' => 'text',
                'label' => __('commons.item.types.labels.text'),
                'notifiable' => true,
                'help' => __('commons.item.types.hints.text'),
            ],
            (object) [
                'value' => 'longtext',
                'label' => __('commons.item.types.labels.longtext'),
                'notifiable' => true,
                'help' => __('commons.item.types.hints.longtext'),
            ],
            (object) [
                'value' => 'datetime',
                'label' => __('commons.item.types.labels.date'),
                'notifiable' => true,
                'help' => __('commons.item.types.hints.date'),
            ],
            (object) [
                'value' => 'select',
                'label' => __('commons.item.types.labels.choice'),
                'notifiable' => true,
                'help' => __('commons.item.types.hints.choice'),
            ],
        ];
    }

    public function getHumanTypeAttribute()
    {
        $types = self::datatypes();

        foreach ($types as $t) {
            if ($t->value == $this->type) {
                return $t->label;
            }
        }

        return '';
    }

    public static function importColumns()
    {
        $type_labels = [];
        foreach (Item::datatypes() as $datatype) {
            $type_labels[] = $datatype->label;
        }

        return [
            (object) [
                'label' => __('commons.utils.name'),
                'description' => __('commons.item.attributes.hints.name'),
            ],
            (object) [
                'label' => __('commons.item.help'),
                'description' => __('commons.item.attributes.hints.help'),
            ],
            (object) [
                'label' => __('commons.item.type'),
                'description' => __('commons.item.attributes.hints.type', ['types' => join(', ', $type_labels)]),
            ],
            (object) [
                'label' => __('commons.item.options'),
                'description' => __('commons.item.attributes.hints.options'),
            ],
        ];
    }

    public static function datatype($type)
    {
        $types = self::datatypes();

        foreach ($types as $t) {
            if ($t->value == $type) {
                return $t;
            }
        }

        return null;
    }

    public function evaluate($value)
    {
        $status = false;

        if ($this->notify == false) {
            return $status;
        }

        try {
            switch ($this->type) {
                case 'boolean':
                    $status = ($value == $this->notificate->when);
                    break;

                case 'number':
                    if ($this->notificate->when == 'minor') {
                        $status = ($value <= $this->notificate->than);
                    }
                    else {
                        $status = ($value >= $this->notificate->than);
                    }
                    break;

                case 'select':
                    $status = in_array($value, $this->notificate->when);
                    break;

                case 'text':
                case 'longtext':
                    if ($this->notificate->when == 'empty') {
                        $status = empty($value);
                    }
                    else {
                        $status = !empty($value);
                    }

                    break;
            }
        }
        catch (\Exception $e) {
            \Log::error('Error in item evaluation: ' . $e->getMessage());
        }

        return $status;
    }

    public function getChoicesTextsAttribute()
    {
        $adjusted_contents = [];
        $ret = [];

        if ($this->type == 'select') {
            $data = json_decode($this->data);
            foreach ($data as $d) {
                if (is_string($d)) {
                    $obj = (object)[
                        'text' => $d,
                        'notify' => false
                    ];

                    $adjusted_contents[] = $obj;
                    $d = $obj;
                }

                $ret[] = $d->text;
            }
        }

        if (!empty($adjusted_contents)) {
            $this->data = json_encode($adjusted_contents);
            $this->save();
        }

        return $ret;
    }

    public function getNotificateAttribute()
    {
        return json_decode($this->notification);
    }

    public function existingData($data)
    {
        if ($data) {
            foreach ($data as $d) {
                if ($d->name == $this->name && isset($d->value) && filled($d->value)) {
                    switch ($this->type) {
                        case 'boolean':
                            if ($d->value == 'false') {
                                return false;
                            }

                            break;

                        case 'select':
                            if ($d->value == __('commons.none')) {
                                return false;
                            }

                            break;
                    }

                    return $d->value;
                }
            }
        }

        return false;
    }
}
