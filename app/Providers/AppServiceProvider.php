<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Mail;
use Illuminate\Pagination\Paginator;
use Symfony\Component\Mailer\Bridge\Scaleway\Transport\ScalewayTransportFactory;
use Symfony\Component\Mailer\Transport\Dsn;
use Storage;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Paginator::useBootstrap();

        $mailer = env('MAIL_MAILER');

        if ($mailer == 'scaleway') {
            Mail::extend('scaleway', function () {
                return (new ScalewayTransportFactory())->create(
                    new Dsn('scaleway+api', 'default', config('mail.mailers.scaleway.username'), config('mail.mailers.scaleway.password'))
                );
            });
        }

        app('translator')->handleMissingKeysUsing(function ($key, $replace, $locale, $fallback) {
            \Log::error('Missing translation key ' . $key . ' for locale ' . $locale);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('reports_disk', function ($app) {
            return Storage::disk(env('REPORTS_STORAGE'));
        });
    }
}
