<?php

return [
    'subjects' => [
        'anomaly' => 'anomalies in checklist :checklist',
        'send_back' => 'your compiled checklist',
        'failsync' => 'unable to sync checklist!',
        'out_of_time' => 'checklist filled out of time: :checklist',
        'reset' => 'keys reset for :user',
    ],

    'key_changed' => 'Signing keys for user :user have changed.',
    'noupload' => 'Warning! The checklist :checklist has not been uploaded!',
    'noupload_error' => 'The reported error is: :error',
    'details' => 'For further details, get a look to the report:',

    'fail' => [
        'missing' => 'Warning! There are missing items in :checklist checklist!',
    ],

    'contact' => 'or contact the email address :email',
    'out_of_time' => 'Warning! The checklist :checklist has been filled out of time!',
    'send_back' => 'Your report has been signed and saved, you find a copy in attachment to this email.',
];
