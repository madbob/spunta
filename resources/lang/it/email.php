<?php

return [
    'subjects' => [
        'anomaly' => 'anomalie nella checklist :checklist',
        'send_back' => 'la tua checklist compilata',
        'failsync' => 'impossibile sincronizzare checklist!',
        'out_of_time' => 'checklist compilata fuori tempo massimo: :checklist',
        'reset' => 'chiavi resettate per :user',
    ],

    'key_changed' => 'Le chiavi per l\'utente :user sono cambiate.',
    'noupload' => 'Attenzione! La checklist :checklist non è stata caricata!',
    'noupload_error' => 'L\'errore riportato è: :error',
    'details' => 'Per ulteriori dettagli, guarda il report:',

    'fail' => [
        'missing' => 'Attenzione! Mancano oggetti nella checklist :checklist!',
    ],

    'contact' => 'o contatta l\'indirizzo mail :email',
    'out_of_time' => 'Attenzione! La checklist :checklist è stata compilata fuori tempo massimo!',
    'send_back' => 'Il tuo report è stato firmato e salvato, ne trovi una copia in allegato a questa email.',
];
