<?php

if (isset($item) && $item != null) {
    $modal_id = sprintf('editItem-%s', $item->id);
    $modal_label = __('commons.item.edit');
    $form_url = route('item.update', $item->id);
    $form_method = 'PUT';
}
else {
    $notice = null;
    $modal_id = 'createItem';
    $modal_label = __('commons.item.create');
    $form_url = route('item.store');
    $form_method = 'POST';
}

$notifications_on = ($item && $item->notify);
$mandatory_on = ($item && $item->mandatory);
$notificate_when = ($item ? ($item->notificate->when ?? 'never') : 'never');

?>

<div class="modal fade item-modal" id="{{ $modal_id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $modal_label }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ $form_url }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="{{ $form_method }}">

                <div class="modal-body">
                    <div class="form-group">
                        <label for="name-{{ $modal_id }}">{{ __('commons.utils.name') }}</label>
                        <input type="text" class="form-control" name="name" id="name-{{ $modal_id }}" value="{{ $item ? $item->name : '' }}" required autocomplete="false">
                    </div>
                    <div class="form-group">
                        <label for="help-{{ $modal_id }}">{{ __('commons.item.help') }}</label>
                        <input type="text" class="form-control" name="help" id="help-{{ $modal_id }}" value="{{ $item ? $item->help : '' }}" autocomplete="false">
                    </div>
                    <div class="form-group select-wrapper">
                        <label for="type-{{ $modal_id }}">{{ __('commons.item.type') }}</label>
                        <select class="form-control" name="type" id="type-{{ $modal_id }}" autocomplete="false">
                            @foreach(App\Item::datatypes() as $datatype)
                                <option value="{{ $datatype->value }}" {{ $item && $item->type == $datatype->value ? 'selected' : '' }}>{{ $datatype->label }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-check form-check-group">
                        <input type="checkbox" class="form-check-input" name="mandatory" id="mandatory-{{ $modal_id }}" autocomplete="false" {{ $mandatory_on ? 'checked' : '' }}>
                        <label for="mandatory-{{ $modal_id }}">{{ __('commons.item.mandatory') }}</label>
                    </div>
                    <div class="form-check form-check-group">
                        <input type="checkbox" class="form-check-input" name="notify" id="notify-{{ $modal_id }}" autocomplete="false" {{ $notifications_on ? 'checked' : '' }}>
                        <label for="notify-{{ $modal_id }}">{{ __('commons.notification.send') }}</label>
                    </div>

                    <div class="extra-settings">
                        <div class="form-group row settings-select {{ (is_null($item) || $item->type != 'select') ? 'd-none' : '' }}">
                            <div class="col-sm-4">{{ __('commons.option.list') }}</div>
                            <div class="col-sm-8">
                                <table class="table dynamic-table">
                                    <tbody class="sortable" data-sorted-handler=".sorting-option">
                                        @if($item)
                                            @foreach($item->choices_texts as $option)
                                                <?php $option_to_check_notify = in_array($option, $item->notificate->when ?? []) ?>

                                                <tr>
                                                    <td>
                                                        <input type="text" name="option[]" value="{{ $option }}" class="form-control border-0">
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" name="notifiable[]" value="{{ $option }}" class="notify-settings {{ $notifications_on ? '' : 'd-none' }}" {{ $option_to_check_notify ? 'checked' : '' }}>
                                                    </td>
                                                    <td class="action-icons">
                                                        <span class="oi oi-trash text-danger remove-row" title="{{ __('commons.option.delete') }}"></span>
                                                        <span class="oi oi-resize-height text-info sorting-option" title="{{ __('commons.utils.drag_sort') }}"></span>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        <tr>
                                            <td colspan="3">
                                                <a href="#" class="btn btn-light add-row">{{ __('commons.option.add') }}</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot hidden>
                                        <tr>
                                            <td>
                                                <input type="text" name="option[]" class="form-control border-0">
                                            </td>
                                            <td>
                                                <input type="checkbox" name="notifiable[]" value="" class="notify-settings {{ $notifications_on ? '' : 'd-none' }}">
                                            </td>
                                            <td>
                                                <span class="oi oi-trash text-danger remove-row" title="{{ __('commons.option.delete') }}"></span>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="notify-settings {{ $notifications_on ? '' : 'd-none' }}">
                        <div class="extra-settings settings-boolean {{ ($item && $item->type != 'boolean') ? 'd-none' : '' }}">
                            <div class="form-check">
                                <input type="radio" class="form-check-input" name="boolean_value" id="boolean_value_true-{{ $modal_id }}" value="true" {{ $notificate_when == 'true' ? 'checked' : '' }}>
                                <label for="boolean_value_true-{{ $modal_id }}">{{ __('commons.notification.condition.true') }}</label>
                            </div>
                            <div class="form-check">
                                <input type="radio" class="form-check-input" name="boolean_value" id="boolean_value_false-{{ $modal_id }}" value="false" {{ $notificate_when == 'false' ? 'checked' : '' }}>
                                <label for="boolean_value_false-{{ $modal_id }}">{{ __('commons.notification.condition.false') }}</label>
                            </div>
                        </div>

                        <div class="extra-settings settings-number {{ (is_null($item) || $item->type != 'number') ? 'd-none' : '' }}">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">{{ __('commons.notification.condition.value') }}</span>
                                </div>
                                <div class="select-wrapper">
                                    <select class="form-control" name="number_value_direction" autocomplete="false">
                                        <option value="minor" {{ $notificate_when == 'minor' ? 'selected' : '' }}>{{ __('commons.notification.condition.value_below') }}</option>
                                        <option value="major" {{ $notificate_when == 'major' ? 'selected' : '' }}>{{ __('commons.notification.condition.value_above') }}</option>
                                    </select>
                                </div>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">{{ __('commons.notification.condition.value_cmp') }}</span>
                                </div>
                                <input type="number" class="form-control" name="number_value_threeshold" autocomplete="false" value="{{ $item && $item->type == 'number' ? ($item->notificate->than ?? 0) : '' }}">
                            </div>
                        </div>

                        <div class="extra-settings settings-text settings-longtext {{ (is_null($item) || $item->type != 'text') ? 'd-none' : '' }}">
                            <div class="form-check">
                                <input type="radio" class="form-check-input" name="text_value" id="text_value_empty-{{ $modal_id }}" value="empty" {{ $notificate_when == 'empty' ? 'checked' : '' }}>
                                <label class="form-check-label" for="text_value_empty-{{ $modal_id }}">{{ __('commons.notification.condition.empty') }}</label>
                            </div>
                            <div class="form-check">
                                <input type="radio" class="form-check-input" name="text_value" id="text_value_nonempty-{{ $modal_id }}" value="nonempty" {{ $notificate_when == 'nonempty' ? 'checked' : '' }}>
                                <label class="form-check-label" for="text_value_nonempty-{{ $modal_id }}">{{ __('commons.notification.condition.not_empty') }}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('commons.close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('commons.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
