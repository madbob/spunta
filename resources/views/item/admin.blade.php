<div class="row">
    <div class="col">
        <button type="button" class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#createItem">{{ __('commons.item.create') }}</button>
        <button type="button" class="btn btn-primary float-end me-2" data-bs-toggle="modal" data-bs-target="#importCSV">{{ __('commons.utils.csv') }}</button>
        <div class="clearfix"></div>
        <hr>
    </div>
</div>

<div class="row">
    <div class="col">
        @if($items->isEmpty())
            <div class="alert alert-info">
                <p>
                    {{ __('commons.item.empty') }}
                </p>
                <p>
                    {{ __('commons.item.explain') }}
                </p>
                <p>
                    {{ __('commons.hints.item_types_list') }}
                </p>
                <ul>
                    @foreach(App\Item::datatypes() as $datatype)
                        <li><strong>{{ $datatype->label }}</strong>: {{ $datatype->help }}</li>
                    @endforeach
                </ul>
            </div>
        @else
            <input type="text" class="form-control filter-table" data-target="#admin-items" placeholder="{{ __('commons.utils.filter') }}">
            <hr>

            <div class="table-responsive">
                <table class="table" id="admin-items">
                    <thead>
                        <tr>
                            <th width="25%">{{ __('commons.utils.name') }}</th>
                            <th width="30%">{{ __('commons.item.help') }}</th>
                            <th width="20%">{{ __('commons.item.type') }}</th>
                            <th width="5%">{{ __('commons.item.notification') }}</th>
                            <th width="5%">{{ __('commons.item.mandatory') }}</th>
                            <th width="15%" class="text-end">{{ __('commons.item.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $item)
                            <tr data-item-id="{{ $item->id }}">
                                <td class="filterable">
                                    {{ $item->name }}
                                </td>
                                <td>
                                    {{ $item->help }}
                                </td>
                                <td>
                                    {{ App\Item::datatype($item->type)->label }}
                                </td>
                                <td class="text-center">
                                    @if($item->notify)
                                        <span class="oi oi-check" title="{{ __('commons.item.notifiable.yes') }}"></span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if($item->mandatory)
                                        <span class="oi oi-check" title="{{ __('commons.item.notifiable.yes') }}"></span>
                                    @endif
                                </td>
                                <td class="text-end">
                                    <span class="action-icons">
                                        <span class="oi oi-pencil text-primary async-modal-edit" data-edit-url="{{ route('item.edit', $item->id) }}"></span>
                                        <span class="oi oi-trash text-danger async-modal-edit" data-edit-url="{{ route('item.askdestroy', $item->id) }}"></span>
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
</div>

@include('item.edit', ['item' => null])

<div class="modal fade" id="importCSV" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('commons.utils.csv') }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('item.import') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="modal-body">
                    <p>
                        {{ __('commons.hints.import_items') }}
                    </p>
                    <ul>
                        @foreach(App\Item::importColumns() as $column)
                            <li>{{ $column->label }}: {{ $column->description }}</li>
                        @endforeach
                    </ul>
                    <p>
                        {!! __('commons.hints.import_items_details', ['url' => route('item.import.sample')]) !!}
                    </p>

                    <div class="form-group mt-4">
                        <input type="file" class="form-control" name="file" id="file" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('commons.close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('commons.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
