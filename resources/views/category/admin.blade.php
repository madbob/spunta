<div class="row">
    <div class="col">
        <button type="button" class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#createCategory">{{ __('commons.category.create') }}</button>
        <div class="clearfix"></div>
    </div>
</div>

@if($categories->isEmpty())
    <hr>

    <div class="alert alert-info">
        <p>
            {{ __('commons.category.empty') }}
        </p>
        <p>
            {{ __('commons.category.explain') }}
        </p>
    </div>
@else
    <div class="sortable" data-sorted-handler=".sorting-categories" data-sorted-callback="updateCategories" data-update-url="{{ route('category.update', 0) }}">
        @foreach($categories as $category)
            <div class="row category-row" data-category-id="{{ $category->id }}" data-update-url="{{ route('category.update', $category->id) }}">
                <div class="col-12 col-md-6">
                    <p class="display-5">{{ $category->name }}</p>
                    <span class="action-icons">
                        <span class="oi oi-pencil text-primary async-modal-edit" title="{{ __('commons.category.edit') }}" data-edit-url="{{ route('category.edit', $category->id) }}"></span>
                        <span class="oi oi-trash text-danger async-modal-edit" title="{{ __('commons.category.delete') }}" data-edit-url="{{ route('category.askdestroy', $category->id) }}"></span>
                        <span class="oi oi-resize-height text-info sorting-categories" title="{{ __('commons.utils.drag_sort') }}"></span>
                    </span>
                </div>
                <div class="col-12 col-md-6">
                    <ul class="list-group sortable" data-sorted-callback="updateChecklists">
                        <?php $count_checklists = $category->checklists->count() ?>
                        @foreach($category->checklists as $checklist)
                            <li class="list-group-item" data-checklist-id="{{ $checklist->id }}">
                                <span class="name">{{ $checklist->name }}</span>
                                <span class="action-icons float-end">
                                    <span class="oi oi-pencil text-primary async-modal-edit" title="{{ __('commons.checklist.edit') }}" data-edit-url="{{ route('checklist.edit', $checklist->id) }}"></span>
                                    <span class="oi oi-trash text-danger async-modal-edit" title="{{ __('commons.checklist.delete') }}" data-edit-url="{{ route('checklist.askdestroy', $checklist->id) }}"></span>
                                    <span class="oi oi-loop-square text-info async-modal-edit" title="{{ __('commons.checklist.duplicate') }}" data-edit-url="{{ route('checklist.create', ['original' => $checklist->id]) }}"></span>
                                    @if($count_checklists > 1)
                                        <span class="oi oi-resize-height text-info" title="{{ __('commons.utils.drag_sort') }}"></span>
                                    @endif
                                </span>
                            </li>
                        @endforeach
                    </ul>

                    <button type="button" class="list-group-item list-group-item-action list-group-item-primary btn-sm" data-bs-toggle="modal" data-bs-target="#createChecklist-{{ $category->id }}">{{ __('commons.checklist.add', ['name' => $category->name]) }}</button>
                    @include('checklist.edit', ['category' => $category, 'checklist' => null])
                </div>
            </div>
        @endforeach
    </div>
@endif

@include('category.edit', ['category' => null])
