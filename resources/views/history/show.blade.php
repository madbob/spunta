@extends('app')

@section('contents')
    <div class="row">
        <div class="col">
            <h2>{{ $report->checklist->name }}</h2>
            <h2>{{ $report->user->name }}</h2>
            <h2>{{ $report->date }}</h2>

            <br>

            <table class="table">
                <?php $data = json_decode($report->as_data) ?>
                @include('history.staticslot', ['master' => $data, 'deep' => 0])
            </table>
        </div>
        <div class="col">
            <div class="card bg-light">
                <div class="card-header">{{ __('commons.report.signed') }}</div>
                <div class="card-body">
                    <p class="card-text">
                        <pre>{{ $report->as_text }}</pre>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
