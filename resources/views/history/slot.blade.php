<h2 class="mt-4">{{ $slot->name }}</h2>

<ul class="list-group">
    @foreach($slot->contents as $content)
        @if(is_a($content, 'App\Slot'))
            <li class="list-group-item">
                @include('history.slot', ['slot' => $content, 'data' => $content->existingData($data)])
            </li>
        @elseif(is_a($content, 'App\Item'))
            <?php

            $value = $content->existingData($data);

            if ($value) {
                $wrapper_class = 'success';
            }
            elseif ($content->type == 'text' || $content->type == 'longtext' || $content->type == 'freetext') {
                $wrapper_class = 'default';
            }
            else {
                $wrapper_class = 'danger';
            }

            ?>

            <li class="list-group-item list-group-item-{{ $wrapper_class }} checklist-filling-row">
                <div class="row">
                    <div class="col">
                        <p>{{ $content->name }}</p>
                        @if(!empty($content->help))
                            <small>{{ $content->help }}</small>
                        @endif

                        @if($content->type == 'longtext' || $content->type == 'freetext')
                            <textarea class="form-control form-control-lg" name="item_{{ $content->id }}" rows="5" {{ $content->mandatory ? 'required' : '' }}>{{ $value }}</textarea>
                        @endif
                    </div>

                    @if($content->type != 'longtext' && $content->type != 'freetext')
                        <div class="col">
                            @if($content->type == 'boolean')
                                <div class="btn-group btn-group-toggle btn-group-lg float-end">
                                    <label class="btn {{ $value == 'true' ? 'active btn-success' : 'btn-light' }}">
                                        <input type="radio" class="btn-check" name="item_{{ $content->id }}" value="true" autocomplete="off" {{ $value == 'true' ? 'checked' : '' }}> {{ __('commons.yes') }}
                                    </label>
                                    <label class="btn {{ $value != 'true' ? 'active btn-danger' : 'btn-light' }}">
                                        <input type="radio" class="btn-check" name="item_{{ $content->id }}" value="false" autocomplete="off" {{ $value != 'true' ? 'checked' : '' }}> {{ __('commons.no') }}
                                    </label>
                                </div>
                            @elseif($content->type == 'number')
                                <input type="number" class="form-control form-control-lg" name="item_{{ $content->id }}" value="{{ $value }}" autocomplete="false" {{ $content->mandatory ? 'required' : '' }}>
                            @elseif($content->type == 'text')
                                <input type="text" class="form-control form-control-lg" name="item_{{ $content->id }}" autocomplete="false" value="{{ $value }}" {{ $content->mandatory ? 'required' : '' }}>
                            @elseif($content->type == 'datetime')
                                <input type="datetime-local" class="form-control form-control-lg" name="item_{{ $content->id }}" autocomplete="false" value="{{ $value }}" {{ $content->mandatory ? 'required' : '' }}>
                            @elseif($content->type == 'select')
                                <select class="form-control form-control-lg" name="item_{{ $content->id }}" autocomplete="false" {{ $content->mandatory ? 'required' : '' }}>
                                    <option value="">{{ __('commons.none') }}</option>
                                    @foreach($content->choices_texts as $option)
                                        <option value="{{ $option }}" {{ $value == $option ? 'selected' : '' }}>{{ $option }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    @endif
                </div>
            </li>
        @endif
    @endforeach
</ul>
