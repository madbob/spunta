@extends('app')

@section('contents')
    <form method="GET" action="{{ route('history.index') }}">
        <div class="row">
            <div class="col">
                <div class="form-group select-wrapper">
                    <label for="user_id">{{ __('commons.report.user') }}</label>
                    <select name="user_id" id="user_id" autocomplete="false">
                        <option value="-1" {{ $actual_user == -1 ? 'selected' : '' }}>{{ __('commons.none') }}</option>

                        @foreach(App\User::orderBy('name', 'asc')->get() as $user)
                            <option value="{{ $user->id }}" {{ $actual_user == $user->id ? 'selected' : '' }}>{{ $user->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group select-wrapper">
                    <label for="checklist_id">{{ __('commons.report.checklist') }}</label>
                    <select name="checklist_id" id="checklist_id" autocomplete="false">
                        <option value="-1" {{ $actual_checklist == -1 ? 'selected' : '' }}>{{ __('commons.none') }}</option>

                        @foreach(App\Checklist::orderBy('name', 'asc')->get() as $checklist)
                            <option value="{{ $checklist->id }}" {{ $actual_checklist == $checklist->id ? 'selected' : '' }}>{{ $checklist->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">{{ __('commons.report.date') }}</span>
                        </div>
                        <input id="search_start" type="date" class="form-control" name="start" value="{{ $actual_start }}">
                        <input id="search_end" type="date" class="form-control" name="end" value="{{ $actual_end }}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">{{ __('commons.report.hour') }}</span>
                        </div>
                        <div class="select-wrapper">
                            <select name="hour_start">
                                <option value="-1" {{ $actual_hour_start == -1 ? 'selected' : '' }}>{{ __('commons.none') }}</option>
                                @foreach(App\Checklist::availableHours() as $hours)
                                    <option value="{{ $hours }}" {{ $actual_hour_start == $hours ? 'selected' : '' }}>{{ $hours }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="select-wrapper">
                            <select name="hour_end">
                                <option value="-1" {{ $actual_hour_end == -1 ? 'selected' : '' }}>{{ __('commons.none') }}</option>
                                @foreach(App\Checklist::availableHours() as $hours)
                                    <option value="{{ $hours }}" {{ $actual_hour_end == $hours ? 'selected' : '' }}>{{ $hours }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-success float-end">{{ __('commons.utils.search') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <hr>

    <div class="row">
        <div class="col">
            @if($reports->isEmpty())
                <div class="alert alert-info">
                    <p>
                        {{ __('commons.report.empty') }}
                    </p>
                </div>
            @else
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="25%">{{ __('commons.report.user') }}</th>
                                <th width="25%">{{ __('commons.report.checklist') }}</th>
                                <th width="25%">{{ __('commons.report.date') }}</th>
                                <th width="5%">{{ __('commons.report.status') }}</th>
                                <th width="20%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($reports as $report)
                                <tr>
                                    <td>{{ $report->user->name }}</td>

                                    <td>
                                        {{ $report->checklist->name }}
                                        @if($report->checklist->trashed())
                                            {{ __('commons.report.statuses.deleted_label') }}
                                        @endif
                                    </td>

                                    <td>{{ printableDateTime($report->date) }}</td>

                                    <td>
                                        @if($report->status)
                                            <span class="oi oi-check" title="{{ __('commons.report.statuses.complete') }}"></span>
                                        @else
                                            <span class="oi oi-ban" title="{{ __('commons.report.statuses.anomaly') }}"></span>
                                        @endif

                                        @if($report->checklist->inTime($report->date) == false)
                                            <span class="oi oi-clock" title="{{ __('commons.report.statuses.expired') }}"></span>
                                        @endif

                                        @if($report->signed == false)
                                            <span class="oi oi-warning" title="{{ __('commons.report.statuses.unsigned') }}"></span>
                                        @endif
                                    </td>
                                    <td>
                                        <span class="action-icons float-end">
                                            <a href="{{ route('history.show', $report->id) }}"><span class="oi oi-magnifying-glass text-info" title="{{ __('commons.report.show') }}"></span></a>
                                            <a href="{{ route('history.download', $report->id) }}"><span class="oi oi-data-transfer-download text-info" title="{{ __('commons.report.download') }}"></span></a>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                {!! $reports->links() !!}
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col">
            <a href="{{ route('history.export', ['user_id' => $actual_user, 'checklist_id' => $actual_checklist, 'start' => $actual_start, 'end' => $actual_end, 'hour_start' => $actual_hour_start, 'hour_end' => $actual_hour_end]) }}" class="btn btn-success float-end">{{ __('commons.report.export') }}</a>
        </div>
    </div>
@endsection
