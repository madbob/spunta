<div class="modal fade" id="destroySlot-{{ $slot->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('commons.slot.delete') }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('slot.destroy', $slot->id) }}" method="POST">
                @csrf
                <input type="hidden" name="_method" value="DELETE">
                <div class="modal-body">
                    <p>
                        {{ __('commons.confirm.delete.slot', ['slot' => $slot->name, 'checklist' => $slot->checklist->name]) }}
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('commons.close') }}</button>
                    <button type="submit" class="btn btn-danger">{{ __('commons.slot.delete') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
