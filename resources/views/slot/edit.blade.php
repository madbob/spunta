<?php

if (isset($slot) && $slot != null) {
    $modal_id = sprintf('editSlot-%s', $slot->id);
    $modal_label = __('commons.slot.edit');
    $form_url = route('slot.update', $slot->id);
    $form_method = 'PUT';
    $actual_name = $slot->name;
    $checklist_id = $slot->checklist_id;
    $parent_id = $slot->parent_id;
}
elseif (isset($parent_slot) && $parent_slot != null) {
    $modal_id = sprintf('createSubSlot-%s', $parent_slot->checklist_id);
    $modal_label = __('commons.slot.create');
    $form_url = route('slot.store');
    $form_method = 'POST';
    $actual_name = '';
    $checklist_id = $parent_slot->checklist_id;
    $parent_id = $parent_slot->id;
}
else {
    $modal_id = sprintf('createSlot-%s', $checklist->id);
    $modal_label = __('commons.slot.create');
    $form_url = route('slot.store');
    $form_method = 'POST';
    $actual_name = '';
    $checklist_id = $checklist->id;
    $parent_id = 0;
}

?>

<div class="modal fade" id="{{ $modal_id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $modal_label }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ $form_url }}" method="POST">
                @csrf

                <input type="hidden" name="parent_id" value="{{ $parent_id }}">
                <input type="hidden" name="checklist_id" value="{{ $checklist_id }}">
                <input type="hidden" name="_method" value="{{ $form_method }}">

                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">{{ __('commons.utils.name') }}</label>
                        <input type="text" class="form-control" name="name" id="name" value="{{ $actual_name }}" required autocomplete="false">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('commons.close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('commons.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
