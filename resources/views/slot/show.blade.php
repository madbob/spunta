<li class="list-group-item droppable-slot" data-slot-id="{{ $slot->id }}" data-attach-url="{{ route('slot.attach', $slot->id) }}">
    <div>
        {{ $slot->name }}

        <span class="action-icons float-end">
            <span class="oi oi-plus text-success async-modal-edit" title="{{ __('commons.slot.create') }}" data-edit-url="{{ route('slot.create', ['parent_id' => $slot->id]) }}"></span>
            <span class="oi oi-pencil text-primary async-modal-edit" title="{{ __('commons.slot.edit') }}" data-edit-url="{{ route('slot.edit', $slot->id) }}"></span>
            <span class="oi oi-trash text-danger async-modal-edit" title="{{ __('commons.slot.delete') }}"data-edit-url="{{ route('slot.askdestroy', $slot->id) }}"></span>
            <span class="oi oi-resize-height text-info" title="{{ __('commons.utils.drag_sort') }}"></span>
        </span>
    </div>

    <hr>

    <ul class="list-group sortable" data-sorted-callback="updateItemsInSlots" data-update-url="{{ route('slot.update', $slot->id) }}">
        @foreach($slot->contents as $content)
            @if(is_a($content, 'App\Slot'))
                @include('slot.show', ['slot' => $content])
            @elseif(is_a($content, 'App\Item'))
                <li class="list-group-item" data-item-id="{{ $content->id }}">
                    <span class="name">{{ $content->name }}</span><br>
                    <small>{{ $content->human_type }}</small>

                    <span class="action-icons float-end">
                        <span class="oi oi-trash text-danger" title="{{ __('commons.item.remove') }}"></span>
                        <span class="oi oi-resize-height text-info" title="{{ __('commons.utils.drag_sort') }}"></span>
                    </span>
                </li>
            @endif
        @endforeach
    </ul>
</li>
