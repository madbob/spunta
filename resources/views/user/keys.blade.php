@extends('app')

@section('contents')
    <div class="row">
        <div class="col">
            <p>
                {{ __('commons.key.explain') }}
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <p id="please-wait" hidden>
                {{ __('commons.utils.wait') }}
            </p>

            <form method="POST" action="{{ route('user.savekeys') }}" class="generate_keys">
                @csrf
                <input type="hidden" name="name" value="{{ $user->name }}">
                <input type="hidden" name="email" value="{{ $user->email }}">

                <div class="form-group row">
                    <label for="new_pin">{{ __('commons.key.pin') }}</label>
                    <input type="password" class="form-control form-control-lg" name="pin" id="new_pin" required autocomplete="false" minlength="5" maxlength="5">
                </div>

                <div class="form-group row">
                    <div class="col-sm-8 col-sm-offset-4">
                        <button type="submit" class="btn btn-success btn-lg">{{ __('commons.save') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
