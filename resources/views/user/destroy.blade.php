<div class="modal fade" id="destroyUser-{{ $user->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('commons.user.delete') }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('user.destroy', $user->id) }}" method="POST">
                @csrf

                <input type="hidden" name="_method" value="DELETE">
                <div class="modal-body">
                    <p>
                        {{ __('commons.confirm.delete.user', ['title' => $user->name]) }}
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('commons.close') }}</button>
                    <button type="submit" class="btn btn-danger">{{ __('commons.user.delete') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
