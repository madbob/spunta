@extends('app')

@section('contents')
    <div class="row">
        <div class="col">
            <button type="button" class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#createUser">{{ __('commons.user.create') }}</button>
            <button type="button" class="btn btn-primary float-end me-2" data-bs-toggle="modal" data-bs-target="#importCSV">{{ __('commons.utils.csv') }}</button>
            <div class="clearfix"></div>
        </div>
    </div>

    <hr>

    @if($users->count() == 1)
        @if(App\Config::getConfig('login') == 'manual')
            <div class="alert alert-info">
                <p>
                    {{ __('commons.user.explain') }}
                </p>
            </div>
        @endif
    @endif

    <div class="row">
        <div class="col">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th width="25%">{{ __('commons.utils.name') }}</th>
                            <th width="20%">{{ __('commons.user.mail') }}</th>
                            <th width="10%">{{ __('commons.roles.users') }}</th>
                            <th width="10%">{{ __('commons.roles.checklists') }}</th>
                            <th width="10%">{{ __('commons.roles.notices') }}</th>
                            <th width="25%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr class="{{ $user->deleted_at != null ? 'text-muted' : '' }}">
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>

                                @foreach(['users', 'checklists', 'notices'] as $permission_level)
                                    <td>
                                        <input type="checkbox" class="check_switch" data-update-url="{{ route('user.setpermission') }}" data-permission="{{ $permission_level }}" data-target-id="{{ $user->id }}" {{ $user->hasPermission($permission_level) ? 'checked' : '' }} autocomplete="off" {{ $user->id == $currentuser->id || $user->deleted_at != null ? 'disabled' : '' }}>
                                    </td>
                                @endforeach

                                <td>
                                    <span class="action-icons float-end">
                                        @if(App\Config::getConfig('login') == 'manual')
                                            <span class="oi oi-pencil text-primary async-modal-edit" title="{{ __('commons.user.edit') }}" data-edit-url="{{ route('user.edit', $user->id) }}"></span>
                                        @endif

                                        @if($user->deleted_at == null)
                                            <span class="oi oi-trash text-danger async-modal-edit" data-edit-url="{{ route('user.askdestroy', $user->id) }}" title="{{ __('commons.user.delete') }}"></span>
                                        @else
                                            <a href="{{ route('user.restore', $user->id) }}"><span class="oi oi-action-undo text-info" title="{{ __('commons.user.enable') }}"></span></a>
                                        @endif

                                        <a href="{{ route('history.index', ['user_id' => $user->id]) }}"><span class="oi oi-list text-info" title="{{ __('commons.user.reports') }}"></span></a>

                                        @if($user->getPrivkeyAttribute() != null)
                                            <span class="oi oi-key text-info async-modal-edit" title="{{ __('commons.key.reset') }}" data-edit-url="{{ route('user.askkeydestroy', $user->id) }}"></span>
                                        @endif

                                        <a href="{{ route('user.getkey', $user->id) }}"><span class="oi oi-data-transfer-download text-info" title="{{ __('commons.user.key') }}"></span></a>
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('user.edit', ['user' => null])

    <div class="modal fade" id="importCSV" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('commons.utils.csv') }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('user.import') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="modal-body">
                        <p>
                            {!! __('commons.hints.import_users', ['url' => url('import_template.csv')]) !!}
                        </p>

                        <div class="form-group">
                            <input type="file" class="form-control" name="file" id="file" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('commons.close') }}</button>
                        <button type="submit" class="btn btn-success">{{ __('commons.save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
