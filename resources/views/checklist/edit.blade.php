<?php

if (isset($checklist) && $checklist != null) {
    $modal_id = sprintf('editChecklist-%s', $checklist->id);
    $modal_label = __('commons.checklist.edit');
    $form_url = route('checklist.update', $checklist->id);
    $form_method = 'PUT';
    $actual_name = $checklist->name;
    $actual_referent = $checklist->notices_recipient;
    $category_id = $checklist->category_id;
}
else {
    $checklist = null;
    $modal_id = sprintf('createChecklist-%s', $category->id);
    $modal_label = __('commons.checklist.create');
    $form_url = route('checklist.store');
    $form_method = 'POST';
    $actual_name = '';
    $actual_referent = '';
    $category_id = $category->id;
}

?>

<div class="modal fade" id="{{ $modal_id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $modal_label }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ $form_url }}" method="POST">
                @csrf

                <input type="hidden" name="category_id" value="{{ $category_id }}">
                <input type="hidden" name="_method" value="{{ $form_method }}">

                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">{{ __('commons.utils.name') }}</label>
                        <input type="text" class="form-control" name="name" id="name" value="{{ $actual_name }}" required autocomplete="false">
                    </div>

                    <div class="form-group">
                        <label for="notices_recipient">{{ __('commons.notice.recipient') }}</label>
                        <input type="text" class="form-control" name="notices_recipient" id="notices_recipient" value="{{ $actual_referent }}" required autocomplete="false">
                        <small class="form-text text-muted">
                            {{ __('commons.hints.notice_recipient') }}
                        </small>
                    </div>

                    @if(App\Config::getConfig('checklist_times'))
                        <div class="form-group mt-4">
                            <table class="table hours-grid">
                                <thead>
                                    <th width="12.5%">&nbsp;</th>
                                    @foreach(App\Checklist::availableDays() as $day => $label)
                                        <th width="12.5%">{{ $label }}</th>
                                    @endforeach
                                </thead>
                                <tbody>
                                    @foreach(App\Checklist::availableHours() as $hour)
                                        <tr>
                                            <th>{{ $hour }}</th>
                                            @foreach(App\Checklist::availableDays() as $day => $label)
                                                <td>
                                                    <input class="form-control" type="checkbox" name="hours[]" value="{{ sprintf('%s_%s', $day, $hour) }}" {{ $checklist && $checklist->hasHour($day, $hour) ? 'checked' : '' }}>
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    @if($checklist)
                        <a class="btn btn-primary" href="{{ route('category.index', ['tab' => 'distribution', 'checklist' => $checklist->id]) }}">{{ __('commons.checklist.admin') }}</a>
                    @endif

                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('commons.close') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('commons.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
