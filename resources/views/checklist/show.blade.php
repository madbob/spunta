@if($checklist == null && $items->isEmpty() && $categories->isEmpty())
    <div class="row">
        <div class="col">
            <div class="alert alert-info">
                {{ __('commons.hints.checklist_intro') }}
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col">
            <div class="alert alert-info">
                {{ __('commons.hints.populate_items') }}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            @if($items->isEmpty())
                <div class="alert alert-info">
                    <p>
                        {{ __('commons.item.empty') }}
                    </p>
                    <p>
                        {!! __('commons.hints.missing_items', ['url' => route('category.index', ['tab' => 'items'])]) !!}
                    </p>
                </div>
            @else
                <input type="text" class="form-control filter-group" data-target="#disposable-items" placeholder="{{ __('commons.utils.filter') }}">
                <br>

                <ul class="list-group" id="disposable-items">
                    @foreach($items as $item)
                        <li class="list-group-item" data-item-id="{{ $item->id }}">
                            <span class="oi oi-arrow-right" title="{{ __('commons.utils.drag_add') }}"></span>
                            <span class="name">{{ $item->name }}</span>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
        <div class="col-md-6">
            @if($checklist == null)
                <div class="alert alert-warning">
                    <p>
                        {{ __('commons.hints.missing_checklist') }}
                    </p>

                    @if($categories->isEmpty())
                        <p>
                            {!! __('commons.hints.missing_category', ['url' => route('category.index', ['tab' => 'categories'])]) !!}
                        </p>
                    @else
                        @if(App\Checklist::count() == 0)
                            <p>
                                {{ __('commons.hints.no_selectable_checklist') }}
                            </p>
                        @else
                            <p>
                                {{ __('commons.hints.select_checklist') }}
                            </p>
                        @endif
                    @endif
                </div>
            @else
                <li class="list-group-item checklist-row" data-update-url="{{ route('checklist.update', $checklist->id) }}">
                    <span class="action-icons float-end">
                        <button type="button" class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#createSlot-{{ $checklist->id }}">{{ __('commons.slot.create') }}</button>
                    </span>

                    <h3>{{ $checklist->name }}</h3>

                    <div class="clearfix"></div>
                    <hr>

                    <ul class="list-group sortable" data-sorted-callback="updateSlots">
                        @foreach($checklist->slots as $slot)
                            @include('slot.show', ['slot' => $slot])
                        @endforeach
                    </ul>
                </li>

                @include('slot.edit', ['checklist' => $checklist, 'slot' => null])
            @endif
        </div>
        <div class="col-md-3">
            @if($categories->isEmpty())
                <div class="alert alert-info">
                    {{ __('commons.category.empty') }}
                </div>
            @else
                <p class="text-end">
                    {{ __('commons.menu.checklists') }}
                </p>
                <div class="nav flex-column nav-pills">
                    @foreach($categories as $category)
                        <a class="list-group-item list-group-item-light">{{ $category->name }}</a>

                        @if($category->checklists->isEmpty())
                            <a class="nav-link">
                                {{ __('commons.hints.no_checklist_in_category') }}
                            </a>
                        @else
                            @foreach($category->checklists as $iter_checklist)
                                <a class="nav-link {{ $checklist != null && $iter_checklist->id == $checklist->id ? 'active' : '' }}" href="{{ route('category.index', ['tab' => 'distribution', 'checklist' => $iter_checklist->id]) }}">
                                    {{ $iter_checklist->name }}
                                </a>
                            @endforeach
                        @endif
                    @endforeach
                </div>
            @endif
        </div>
    </div>

    <div id="templates" hidden>
        <ul class="item-in-slot">
            <li class="list-group-item" data-item-id="">
                <span class="name"></span>
                <span class="action-icons float-end">
                    <span class="oi oi-trash text-danger" title="{{ __('commons.utils.remove') }}"></span>
                    <span class="oi oi-resize-height text-info" title="{{ __('commons.utils.drag_sort') }}"></span>
                </span>
            </li>
        </ul>
    </div>
@endif
