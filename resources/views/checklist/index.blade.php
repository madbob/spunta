@extends('app')

@section('contents')
    @if($pending->isEmpty() == false)
        <div class="row">
            <div class="col">
                <div class="list-group mb-4">
                    <a href="#" class="list-group-item list-group-item-danger">{{ __('commons.checklist.pending') }}</a>

                    @foreach($pending as $pend)
                        <a href="{{ route('history.edit', ['history' => $pend->id]) }}" class="list-group-item list-group-item-action">
                            {{ $pend->checklist->name }}

                            <small class="float-end">
                                {{ __('commons.checklist.lastupdate', ['date' => date('d/m/Y G:i:s', strtotime($pend->updated_at))]) }}
                            </small>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col">
            @if($categories->isEmpty())
                <div class="alert alert-info">
                    <p>
                        {{ __('commons.checklist.empty') }}
                    </p>
                    @if(Auth::user()->hasPermission('checklists'))
                        <p>
                            {!! __('commons.hints.create_checklist', ['url' => route('category.index')]) !!}
                        </p>
                        @endif
                </div>
            @else
                @foreach($categories as $category)
                    <div class="list-group mb-4">
                        <a href="#" class="list-group-item list-group-item-action active">{{ $category->name }}</a>

                        @foreach($category->checklists as $checklist)
                            <a href="{{ route('history.create', ['checkid' => $checklist->id]) }}" class="list-group-item list-group-item-action">
                                {{ $checklist->name }}

                                <small class="float-end">
                                    {{ __('commons.checklist.lastfill', ['date' => $checklist->histories()->count() == 0 ? __('commons.utils.never') : date('d/m/Y G:i:s', strtotime($checklist->histories()->first()->date))]) }}
                                </small>
                            </a>
                        @endforeach
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection
