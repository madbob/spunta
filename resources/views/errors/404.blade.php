@extends('app')

@section('contents')
    <div class="row justify-content-center mt-4">
        <div class="col-12 col-md-6 text-center">
            <p class="display-1">{{ __('commons.404') }}</p>
            <a href="{{ route('notice.index') }}">{{ __('commons.utils.gotohome') }}</a>
        </div>
    </div>
@endsection
