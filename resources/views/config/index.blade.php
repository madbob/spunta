@extends('app')

@section('contents')
    <div class="row mt-4">
        <div class="col">
            <form method="POST" action="{{ route('config.update', 0) }}" enctype="multipart/form-data">
                @csrf

                <input type="hidden" name="_method" value="PUT">

                <div class="form-group">
                    <label for="intro_message">{{ __('commons.config.public_message') }}</label>
                    <textarea class="form-control" name="intro_message" id="intro_message" autocomplete="false" rows="10" placeholder="{{ __('commons.config.hints.public_message') }}">{{ App\Config::getConfig('intro_message') }}</textarea>
                </div>

                <div class="form-group">
                    <label for="logo" class="active">{{ __('commons.config.logo') }}</label>
                    <input type="file" class="form-control" name="logo" id="logo" accept=".jpg">

                    @if(file_exists(images_path() . 'logo.jpg'))
                        <img src="{{ route('config.image') . '?t=' . time() }}" alt="Logo" class="img-fluid">
                    @endif

                    <small class="form-text text-muted">{{ __('commons.config.hints.logo') }}</small>
                </div>

                <?php $language = App\Config::getConfig('language') ?>
                <div class="form-group select-wrapper">
                    <label for="language">{{ __('commons.config.language') }}</label>
                    <select name="language" id="language">
                        <option value="it_IT" {{ $language == 'it_IT' ? 'selected' : '' }}>Italiano</option>
                        <option value="en_US" {{ $language == 'en_US' ? 'selected' : '' }}>English</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="notify_sync_reports">{{ __('commons.config.main_recipient') }}</label>
                    <input type="email" class="form-control" name="notify_sync_reports" id="notify_sync_reports" placeholder="{{ __('commons.config.hints.main_recipient') }}" value="{{ App\Config::getConfig('notify_sync_reports') }}" autocomplete="false">
                </div>

                <div class="form-check form-check-group">
                    <input class="form-check-input" type="checkbox" name="checklist_times" id="checklist_times" {{ App\Config::getConfig('checklist_times') ? 'checked' : '' }}>
                    <label for="checklist_times" class="form-check-label">{{ __('commons.config.use_temporal_validation') }}</label>
                    <small class="form-text text-muted">{{ __('commons.config.hints.use_temporal_validation') }}</small>
                </div>

                <div class="form-check form-check-group">
                    <input class="form-check-input" type="checkbox" name="self_verification" id="self_verification" {{ App\Config::getConfig('self_verification') ? 'checked' : '' }}>
                    <label for="self_verification" class="form-check-label">{{ __('commons.config.send_back') }}</label>
                    <small class="form-text text-muted">{{ __('commons.config.hints.send_back') }}</small>
                </div>

                <?php

                $has_ftp = App\Config::getConfig('ftp_push');

                ?>

                <div class="form-check form-check-group">
                    <input class="form-check-input" type="checkbox" name="enable_ftp_push" id="enable_ftp_push" {{ $has_ftp->enable ? 'checked' : '' }}>
                    <label for="enable_ftp_push">{{ __('commons.config.ftp') }}</label>
                    <small class="form-text text-muted">{{ __('commons.config.hints.ftp') }}</small>
                </div>

                <div class="ftp-settings card-wrapper card-space mb-4 {{ $has_ftp->enable == false ? 'd-none' : '' }}">
                    <div class="card card-bg">
                        <div class="card-body">
                            <div class="form-group mt-3">
                                <label for="ftp_host">{{ __('commons.config.ftp_details.host') }}</label>
                                <input type="text" class="form-control" name="ftp_host" id="ftp_host" value="{{ $has_ftp->host }}" autocomplete="false">
                            </div>
                            <div class="form-group">
                                <label for="ftp_username">{{ __('commons.config.ftp_details.username') }}</label>
                                <input type="text" class="form-control" name="ftp_username" id="ftp_username" value="{{ $has_ftp->username }}" autocomplete="false">
                            </div>
                            <div class="form-group">
                                <label for="ftp_password">{{ __('commons.config.ftp_details.password') }}</label>
                                <input type="password" class="form-control" name="ftp_password" id="ftp_password" value="{{ $has_ftp->password }}" autocomplete="false">
                            </div>
                            <div class="form-group">
                                <label for="ftp_port">{{ __('commons.config.ftp_details.port') }}</label>
                                <input type="number" class="form-control" name="ftp_port" id="ftp_port" value="{{ $has_ftp->port }}" autocomplete="false">
                            </div>
                            <div class="form-group">
                                <label for="ftp_root">{{ __('commons.config.ftp_details.root') }}</label>
                                <input type="text" class="form-control" name="ftp_root" id="ftp_root" value="{{ $has_ftp->root }}" autocomplete="false">
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="ftp_ssl" id="ftp_ssl" {{ $has_ftp->ssl ? 'checked' : '' }}>
                                <label for="ftp_ssl">{{ __('commons.config.ftp_details.ssl') }}</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="ftp_passive" id="ftp_passive" {{ $has_ftp->passive ? 'checked' : '' }}>
                                <label for="ftp_passive">{{ __('commons.config.ftp_details.passive') }}</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-8 offset-sm-4">
                        <button type="submit" class="btn btn-success float-end">{{ __('commons.save') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
