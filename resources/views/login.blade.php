@extends('app')

@section('contents')
    <div class="row justify-content-center mt-4">
        <div class="col-12 col-md-6 text-center">
            @php
            $message = App\Config::getConfig('intro_message');
            @endphp

            @if(filled($message))
                <div class="alert alert-info mb-5">
                    {!! nl2br($message) !!}
                </div>
            @endif

            @switch(App\Config::getConfig('login'))
                @case('manual')
                    <form action="{{ route('login.manual') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email">{{ __('commons.user.mail') }}</label>
                            <input type="email" class="form-control" name="email" id="email">
                        </div>
                        <div class="form-group">
                            <label for="password">{{ __('commons.user.password') }}</label>
                            <input type="password" class="form-control" name="password" id="password">
                        </div>
                        <button type="submit" class="btn btn-primary">{{ __('commons.menu.login') }}</button>
                    </form>

                    @break

                @case('google')
                    <form action="{{ route('login.google') }}" method="POST">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary">{{ __('commons.menu.login') }}</button>
                        <br>
                        <br>
                        <br>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="remember" id="remember">
                            <label for="remember">{{ __('commons.hints.no_google_logout') }}</label>
                        </div>
                    </form>

                    @break
            @endswitch
        </div>
    </div>
@endsection
