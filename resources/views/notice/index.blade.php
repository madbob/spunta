@extends('app')

@section('contents')
    <div class="row">
        <div class="col">
            @if($notices->isEmpty())
                <div class="alert alert-info">
                    {{ __('commons.notice.empty') }}
                </div>
            @else
                @foreach($notices as $notice)
                    @include('notice.partial.card', ['notice' => $notice, 'dismiss' => true])
                @endforeach
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col text-center">
            <a class="btn btn-light btn-lg" href="{{ route('notice.archive') }}">{{ __('commons.notice.archive') }}</a>
        </div>
    </div>
@endsection
