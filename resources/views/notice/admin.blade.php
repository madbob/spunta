@extends('app')

@section('contents')
    <div class="row">
        <div class="col">
            <button type="button" class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#createNotice">{{ __('commons.notice.create') }}</button>
            <div class="clearfix"></div>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col">
            @if($notices->isEmpty())
                <div class="alert alert-info">
                    <p>
                        {{ __('commons.notice.empty') }}
                    </p>
                    <p>
                        {{ __('commons.notice.explain') }}
                    </p>
                </div>
            @else
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="30%">{{ __('commons.notice.title') }}</th>
                                <th width="10%">{{ __('commons.notice.start') }}</th>
                                <th width="10%">{{ __('commons.notice.end') }}</th>
                                <th width="10%">{{ __('commons.notice.type') }}</th>
                                <th width="15%">{{ __('commons.notice.users') }}</th>
                                <th width="15%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($notices as $notice)
                                <tr class="{{ $notice->archive != null && $notice->archive <= $now ? 'text-muted' : '' }}">
                                    <td>{{ $notice->title }}</td>
                                    <td>{{ printableDate($notice->start) }}</td>
                                    <td>{{ printableDate($notice->end) }}</td>
                                    <td>
                                        @if($notice->type == 'important')
                                            <span class="oi oi-warning"></span>
                                        @else
                                            <span class="oi oi-bullhorn"></span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($notice->users()->count() != 0)
                                            <a href="#" class="async-modal-edit" data-edit-url="{{ route('notice.readcount', $notice->id) }}">{{ $notice->users()->count() }}</a>
                                        @else
                                            0
                                        @endif
                                    </td>
                                    <td>
                                        <span class="action-icons float-end">
                                            <span class="oi oi-pencil text-primary async-modal-edit" title="{{ __('commons.notice.edit') }}" data-edit-url="{{ route('notice.edit', $notice->id) }}"></span>
                                            <span class="oi oi-trash text-danger async-modal-edit" title="{{ __('commons.notice.delete') }}" data-edit-url="{{ route('notice.askdestroy', $notice->id) }}"></span>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col">
            {!! $notices->links() !!}
        </div>
    </div>

    @include('notice.edit', ['notice' => null])
@endsection
