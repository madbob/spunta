<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('commons.notice.users') }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>{{ __('commons.utils.name') }}</th>
                            <th>{{ __('commons.user.mail') }}</th>
                            <th>{{ __('commons.utils.date') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($notice->users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ printableDateTime($user->pivot->date) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('commons.close') }}</button>
            </div>
        </div>
    </div>
</div>
