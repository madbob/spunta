@extends('app')

@section('contents')
    <div class="row">
        <div class="col">
            @if($notices->isEmpty())
                <div class="alert alert-info">
                    {{ __('commons.notice.empty') }}
                </div>
            @else
                @foreach($notices as $notice)
                    @include('notice.partial.card', ['notice' => $notice, 'dismiss' => false])
                @endforeach
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col">
            {!! $notices->links() !!}
        </div>
    </div>
@endsection
