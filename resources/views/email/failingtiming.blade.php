{{ __('email.out_of_time', ['checklist' => $report->checklist->name]) }}

{{ __('email.details') }}
{{ route('history.show', $report->id) }}
{{ __('email.contact', ['email' => $report->user->email]) }}
