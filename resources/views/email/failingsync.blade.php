{{ __('email.noupload', ['checklist' => $report->checklist->name]) }}

{{ __('email.noupload_error', ['error' => $message]) }}

{{ __('email.details') }}
{{ route('history.show', $report->id) }}
