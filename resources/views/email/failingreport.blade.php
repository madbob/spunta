{{ __('email.fail.missing', ['checklist' => $report->checklist->name]) }}

@foreach($issues as $i)
    {{ $i }}
@endforeach

{{ __('email.details') }}
{{ route('history.show', $report->id) }}
{{ __('email.contact', ['email' => $report->user->email]) }}
