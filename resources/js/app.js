import './bootstrap';

import 'jquery-ui/ui/version.js';
import 'jquery-ui/ui/plugin.js';
import 'jquery-ui/ui/widget.js';
import 'jquery-ui/ui/widgets/mouse.js';
import 'jquery-ui/ui/data.js';
import 'jquery-ui/ui/scroll-parent.js';
import 'jquery-ui/ui/widgets/sortable.js';
import 'jquery-ui/ui/widgets/draggable.js';
import 'jquery-ui/ui/widgets/droppable.js';

import { on } from 'codemirror';
import * as openpgp from "openpgp";
import jBob from "jbob";
import 'summernote/dist/summernote-bs5';

class Spunta {
    static updateItemsInSlots(list) {
        var items = new Array();
        $(list).find('> li.list-group-item').each(function() {
            var item_id = $(this).attr('data-item-id');
            if (typeof item_id !== typeof undefined && item_id !== false) {
                items.push('item_' + item_id);
            } else {
                var slot_id = $(this).attr('data-slot-id');
                if (typeof slot_id !== typeof undefined && slot_id !== false)
                    items.push('slot_' + slot_id);
            }
        });

        var url = $(list).attr('data-update-url');

        $.ajax({
            url: url,
            method: 'PUT',
            data: {
                ids: items,
                saving_type: 'sort_items',
                _token: $('meta[name=csrf-token]').attr('content')
            }
        });
    }

    static updateSlots(list) {
        var slots = new Array();
        $(list).find('.droppable-slot').each(function() {
            slots.push($(this).attr('data-slot-id'));
        });

        var cat = $(list).closest('.checklist-row');
        var url = cat.attr('data-update-url');

        $.ajax({
            url: url,
            method: 'PUT',
            data: {
                ids: slots,
                saving_type: 'sort_slots',
                _token: $('meta[name=csrf-token]').attr('content')
            }
        });
    }

    static updateChecklists(list) {
        var checklists = new Array();
        $(list).find('li.list-group-item').each(function() {
            checklists.push($(this).attr('data-checklist-id'));
        });

        var cat = $(list).closest('.category-row');
        var url = cat.attr('data-update-url');

        $.ajax({
            url: url,
            method: 'PUT',
            data: {
                ids: checklists,
                saving_type: 'sort_checklists',
                _token: $('meta[name=csrf-token]').attr('content')
            }
        });
    }

    static updateCategories(list) {
        var categories = new Array();
        $(list).find('.category-row').each(function() {
            categories.push($(this).attr('data-category-id'));
        });

        var url = $(list).attr('data-update-url');

        $.ajax({
            url: url,
            method: 'PUT',
            data: {
                ids: categories,
                saving_type: 'sort_categories',
                _token: $('meta[name=csrf-token]').attr('content')
            }
        });
    }

    static initEditor(modal) {
        modal.find('textarea.htmledit:not(.wysiwyg)').each(function() {
            $(this).summernote({
                height: 200,
                dialogsInBody: true,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'strikethrough']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['link', ['linkDialogShow']]
                ],
            });

            $(this).addClass('wysiwyg');
        });
    }
}

$(document).ready(function() {
    var jbob = new jBob();
    jbob.init({
        initFunction: function(container) {
            $('.sortable', container).each(function() {
                let item = $(this);

                item.sortable({
                    handle: item.attr('data-sorted-handler') ? item.attr('data-sorted-handler') : 'span.oi-resize-height',
                    update: function(event, ui) {
                        event.stopPropagation();
                        var callback = $(this).attr('data-sorted-callback');
                        if (callback) {
                            Spunta[callback](event.target);
                        }
                    },
                    helper: function (e, ui) {
                        ui.children().each(function () {
                            $(this).width($(this).width());
                        });
                        return ui;
                    }
                });
            });

            Spunta['initEditor'](container);
            container.find('input').change();
        },
        fixBootstrap: ['Tooltip'],
    });

    $('.async-modal-edit').click(function() {
        var url = $(this).attr('data-edit-url');

        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'HTML',
            success: function(data) {
                var d = $(data);
                $('body').append(d);
                jbob.initElements(d);
                d.modal('show');
            }
        });
    });

    $('.check_switch').change(function() {
        var url = $(this).attr('data-update-url');
        var target_id = $(this).attr('data-target-id');
        var permission = $(this).attr('data-permission');
        var flag = $(this).prop('checked');

        $.ajax({
            url: url,
            method: 'POST',
            data: {
                target_id: target_id,
                flag: flag,
                permission: permission,
                _token: $('meta[name=csrf-token]').attr('content')
            }
        });
    });

    /*
        Notices
    */

    $('body').on('hidden.bs.modal', '.notice-editor', function() {
        if ($(this).hasClass('editing-notice')) {
            $(this).remove();
        }
    });

    $('.notice').on('close.bs.alert', function() {
        var url = $(this).attr('data-read-url');

        $.ajax({
            url: url,
            method: 'POST',
            data: {
                _token: $('meta[name=csrf-token]').attr('content')
            }
        });
    });

    /*
        Signatures
    */

    $('form.generate_keys').submit(function(e) {
        e.preventDefault();

        var form = $(this);
        form.hide();
        $('#please-wait').removeAttr('hidden');

        var user_pin = form.find('input[name=pin]').val();
        var user_name = form.find('input:hidden[name=name]').val();
        var user_email = form.find('input:hidden[name=email]').val();

        if (user_pin.length == 5) {
            var options = {
                userIDs: [{
                    name: user_name,
                    email: user_email
                }],
                rsaBits: 2048,
                passphrase: user_pin
            };

            openpgp.generateKey(options).then(function(key) {
                var privkey = key.privateKey;
                var pubkey = key.publicKey;

                $.ajax({
                    url: form.attr('action'),
                    method: form.attr('method'),
                    data: {
                        public: pubkey,
                        private: privkey,
                        _token: $('meta[name=csrf-token]').attr('content')
                    },
                    success: function() {
                        window.location = '/';
                    }
                });
            });
        }
    });

    $('form.sign_document').submit(function(e) {
        e.preventDefault();

        var form = $(this);
        form.hide();
        $('#please-wait').removeAttr('hidden');
        $('#error-msg').attr('hidden', 'hidden');

        var privkey = $('#privkey').val();
        var passphrase = form.find('input[name=pin]').val();

        openpgp.readPrivateKey({
            armoredKey: privkey
        }).then(function(privateKey) {
            openpgp.decryptKey({
                privateKey: privateKey,
                passphrase
            }).then(
                function(privKeyObj) {
                    openpgp.createCleartextMessage({
                        text: $('#cleartext').val()
                    }).then(function(unsignedMessage) {
                        openpgp.sign({
                            message: unsignedMessage,
                            signingKeys: privKeyObj
                        }).then(function(cleartext) {
                            $.ajax({
                                url: form.attr('action'),
                                method: form.attr('method'),
                                data: {
                                    signed: cleartext,
                                    _token: $('meta[name=csrf-token]').attr('content')
                                },
                                success: function() {
                                    window.location = '/';
                                }
                            });
                        });
                    });
                },
                /*
                    If the PIN is invalid
                */
                function() {
                    form.show();
                    $('#please-wait').attr('hidden', 'hidden');
                    $('#error-msg').removeAttr('hidden');
                }
            );
        });
    });

    /*
        Items Admin
    */

    $('body').on('change', '.item-modal select[name=type]', function() {
        var opt = $(this).find('option:selected');
        var type = opt.val();
        var notify_settings = $(this).closest('.modal').find('.notify-settings');
        notify_settings.find('> .extra-settings').addClass('d-none').filter('.settings-' + type).removeClass('d-none');
        var extra_settings = $(this).closest('.modal').find('.extra-settings');
        extra_settings.find('> .form-group').addClass('d-none').filter('.settings-' + type).removeClass('d-none');
    })
    .on('change', '.item-modal input[name=notify]', function() {
        $(this).closest('.modal').find('.notify-settings').toggleClass('d-none', $(this).prop('checked') == false);
    })
    .on('keyup', 'input:text[name="option[]"]', function() {
        $(this).closest('tr').find('.notify-settings').val($(this).val());
    });

    /*
        Disposing Panel
    */

    $('.filter-group').keyup(function() {
        var list = $($(this).attr('data-target'));
        var text = $(this).val().toLowerCase();

        if (text == '') {
            list.find('.list-group-item').show();
        } else {
            list.find('.list-group-item').each(function() {
                var test = ($(this).text().toLowerCase().indexOf(text) != -1);
                if (test)
                    $(this).show();
                else
                    $(this).hide();
            });
        }
    });

    $('.filter-table').keyup(function() {
        var table = $($(this).attr('data-target'));
        var text = $(this).val().toLowerCase();

        if (text == '') {
            table.find('tbody tr').show();
        } else {
            table.find('tbody tr').each(function() {
                var test = ($(this).find('.filterable').first().text().toLowerCase().indexOf(text) != -1);
                if (test)
                    $(this).show();
                else
                    $(this).hide();
            });
        }
    });

    $('#disposable-items li').draggable({
        helper: 'clone',
        revert: true,
    });

    $('.droppable-slot').droppable({
        accept: 'li.ui-draggable',
        greedy: true,
        tolerance: 'pointer',
        over: function() {
            $(this).addClass('bg-success');
        },
        out: function() {
            $(this).removeClass('bg-success');
        },
        deactivate: function() {
            $(this).removeClass('bg-success');
        },
        drop: function(event, ui) {
            event.stopPropagation();
            var list = $(this).find('> .list-group');
            var item = ui.draggable.attr('data-item-id');

            /*
                This is to skip this callback when dragging an existing item to
                be sorted
            */
            if (list.find('> li[data-item-id=' + item + ']').length != 0)
                return;

            var url = $(this).attr('data-attach-url');

            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    item: item,
                    revert: 'false',
                    _token: $('meta[name=csrf-token]').attr('content')
                },
                success: function() {
                    var node = $('#templates .item-in-slot li').clone();
                    node.attr('data-item-id', ui.draggable.attr('data-item-id'));
                    node.find('.name').text(ui.draggable.find('.name').text());
                    node.appendTo(list);
                }
            });
        }
    });

    $('.droppable-slot').on('click', '.list-group-item .oi-trash', function() {
        var node = $(this).closest('li');
        var item = node.attr('data-item-id');

        if (item) {
            var url = $(this).closest('.droppable-slot').attr('data-attach-url');

            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    item: item,
                    revert: 'true',
                    _token: $('meta[name=csrf-token]').attr('content')
                },
                success: function() {
                    node.remove();
                }
            });
        }
    });

    /*
        Checklist Fill
    */

    $('.checklist-filling-row').on('change', 'input:radio', function() {
        var row = $(this).closest('li');
        var button = $(this).closest('.btn');

        var selected = $(this).val();
        if (selected == 'true') {
            row.removeClass('list-group-item-danger').addClass('list-group-item-success');
            button.removeClass('btn-light').addClass('btn-success');
            button.siblings('.btn').removeClass('btn-danger').addClass('btn-light');
        } else {
            row.removeClass('list-group-item-success').addClass('list-group-item-danger');
            button.removeClass('btn-light').addClass('btn-danger');
            button.siblings('.btn').removeClass('btn-success').addClass('btn-light');
        }
    })
    .on('change', 'select', function() {
        var row = $(this).closest('li');
        var selected = $(this).find('option:selected').val();

        if (selected == '') {
            row.removeClass('list-group-item-success').addClass('list-group-item-danger');
        } else {
            row.removeClass('list-group-item-danger').addClass('list-group-item-success');
        }
    })
    .on('change', 'input[type=number]', function() {
        var row = $(this).closest('li');
        var selected = $(this).val();

        if (selected == '' || selected == '0') {
            $(this).val('0');
            row.removeClass('list-group-item-success').addClass('list-group-item-danger');
        } else {
            row.removeClass('list-group-item-danger').addClass('list-group-item-success');
        }
    })
    .on('change', 'input[type="datetime-local"]', function() {
        var row = $(this).closest('li');

        if ($(this).val() == '') {
            row.removeClass('list-group-item-success').addClass('list-group-item-danger');
        } else {
            row.removeClass('list-group-item-danger').addClass('list-group-item-success');
        }
    });

    let filling = $('.checklist-create');
    if (filling.length != 0) {
        history.pushState(null, null, location.href);
        window.onpopstate = function () {
            history.go(1);
        };
    }

    /*
        Configurations
    */

    $('#enable_ftp_push').change(function() {
        $('.ftp-settings').toggleClass('d-none', $(this).prop('checked') == false);
    });
});
