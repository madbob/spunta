<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__)
    ->exclude('vendor')
    ->exclude('storage')
;

$config = new PhpCsFixer\Config();
return $config->setRules([
    '@PSR12' => true,
    'control_structure_continuation_position' => [
        'position' => 'next_line',
    ],
])->setFinder($finder);
